// Fungsi ketika page dibuka
$.ajax("/story8/all")
    .done((obj) => {
        const data = obj.items;
        let books = document.getElementById("books");
        books.innerHTML = data.map((objData) => {
            return `<tr id="${objData.id}">
                    ${(objData.volumeInfo.imageLinks !== undefined ?
                                `<td><img src="${objData.volumeInfo.imageLinks.thumbnail}" alt="Thumbnail" class="img-fluid" style="max-height: 100%"></td>` :
                                `<td><p class="col-4 text-center align-self-center">No Thumbnail</p></td>`)}
                        <td><h5 style="font-weight: bold"><a target="_blank" href="${objData.volumeInfo.infoLink}" style="color:black;">${objData.volumeInfo.title}</a></h5></td>
                        <td><p>${(objData.volumeInfo.description !== undefined ? objData.volumeInfo.description: "None")}</p></td>
                        <td><ul>
                        ${(objData.volumeInfo.authors === undefined ? "None" : objData.volumeInfo.authors.map((author) => {
                            return `<li style="font-size: 0.9rem">${author}</li>`
                            }
                        ).join(" "))}
                        </ul></td>
                </tr>`
        }).join('');
    })
    .fail(() => {
        let books = document.getElementById("books");
        books.innerHTML = `
            <tr>
                <th class="text-center" colspan="4">
                    <h3 class="text-center">Something Error</h3>
                </th>
            </tr>
            `
    });

//Fungsi saat tombol search diklik
const changePage = () => {
    let input = document.getElementById("search");
    if(input.value !== ""){
        $.ajax("/story8/"+input.value)
        .done((obj) => {
            const data = obj.items;
            let books = document.getElementById("books");
            books.innerHTML = data.map((objData) => {
                return `<tr id="${objData.id}">
                        ${(objData.volumeInfo.imageLinks !== undefined ?
                                    `<td><img src="${objData.volumeInfo.imageLinks.thumbnail}" alt="Thumbnail" class="img-fluid" style="max-height: 100%"></td>` :
                                    `<td><p class="col-4 text-center align-self-center">No Thumbnail</p></td>`)}
                            <td><h5 style="font-weight: bold"><a target="_blank" href="${objData.volumeInfo.infoLink}" style="color:black;">${objData.volumeInfo.title}</a></h5></td>
                            <td><p>${(objData.volumeInfo.description !== undefined ? objData.volumeInfo.description: "None")}</p></td>
                            <td><ul>
                            ${(objData.volumeInfo.authors === undefined ? "None" : objData.volumeInfo.authors.map((author) => {
                                return `<li style="font-size: 0.9rem">${author}</li>`
                                }
                            ).join(" "))}
                            </ul></td>
                        </tr>`
            }).join('');
        })
        .fail(() => {
            let books = document.getElementById("books");
            books.innerHTML = `
                <tr>
                    <th class="text-center" colspan="4">
                        <h3 class="text-center">Something Error</h3>
                    </th>
                </tr>
                `
        });

        input.value = "";
        input.dispatchEvent(new Event("input"));
    }
}
