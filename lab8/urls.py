from .views import *
from django.shortcuts import render
from django.urls import path
from lab8.views import google_books, lab8View

app_name = 'lab8'
urlpatterns = [
    path('', lab8View, name='lab8'),
    path('/<str:keyword>',google_books)
]
