from django.shortcuts import render
import requests
from django.http import HttpResponse, JsonResponse

# Create your views here.
def lab8View(request):
    return render(request,'story8.html')

def google_books(request,keyword):
    response = requests.get('https://www.googleapis.com/books/v1/volumes?q='+keyword)
    return JsonResponse(response.json())